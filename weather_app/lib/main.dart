import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main()
{

  //maintaine the portrait mode
  SystemChrome.setPreferredOrientations(
    [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]
  ).then((_) => MyApp(),); 
}

class MyApp extends StatelessWidget {

  const MyApp({Key key}) : super(key:key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Weather App',
      debugShowCheckedModeBanner: false,
    );
  }
}




